# ios-test



## Test technique iOS Prisma Media

### Introduction

Le but de ce test est de créer une application simple. Cette application doit nous permettre d'évaluer l'état de vos connaissances sur la plateforme iOS. Aucune limite de temps n'est imposée pour réaliser ce test (dans la limite du raisonnable). Il n'y a donc aucune raison de se presser. Favorisez la qualité à la quantité.

Vous devrez générer un nouveau projet à partir de XCode 15, utiliser la dernière version de swift et SPM pour la gestion de vos dépendances.

Vous êtes libre d’utiliser les libraires externes de votre choix si nécessaire (sauf si elle concerne l’UI, ou les appels réseaux). À vous également de choisir l'architecture que vous pensez la plus adaptée. Attendez-vous à être questionné sur ces choix.

### Données fournies 

Les données de l’application seront fournies par le contenu du fichier JSON `get_service_chords.json`
 
Ce dernier contient :
• Une liste note
• Une liste d’accords associé à chaque note
• Une liste de position de doigts sur un manche de guitare associé à chaque accord

### Consignes

L'application devra comporter quatre écrans, aucun système d'authentification n'est requis. L'UI de l'application ne vous est pas imposée, laissez parler votre créativité, sinon referez-vous au screenshots fournis. Cependant ce n'est pas l'élément principal sur lequel vous serez jugé, privilégiez la qualité du code et la pertinence de vos choix techniques.

![chord](image.png)



### Écran - Chords :

Le but de cet écran est d’afficher tous les accords possibles, en fonction d’une note particulière :
1. Afficher la liste des notes retournées par le json (contenu de « allkeys
») dans une vue
«
pager » horizontale.
2. A chaque changement de la note, la liste des accords doit se mettre à jours avec le contenu
de « keychordids
». C’est également une vue pager horizontale. Chaque id contenu dans
ce tableau, fait référence à une entrée dans « allchords » par « chordid »

### Les autres écrans :

Ce sont des écrans ayant aucun contenu

### Contraintes

Vous devrez absolument respecter les contraintes suivantes :
#### Obligatoire :
- L'application devra être compatible iOS 14 minimum ;
- L’intégralité de l’application sera développée en SwiftUI (dépendances comprises).
- L’application doit être testée.
- Utiliser le moteur d’injection Swinject.
- L'application doit absolument compiler, 0 warning sera fortement apprécié.
#### Optionnel :
- Commenter judicieusement votre code en anglais là où cela vous semblera nécessaire
- La position des doigts, doit se mettre à jour, à chaque changement d’accord. Modifiez la
position des doigts en fonction du contenu du tableau « fingers » associé à l’accord
sélectionné. La première entrée du tableau correspond à la première corde du bas, et la
dernière entrée du tableau, à la dernière corde du haut. La valeur indique le numéro de la
case, ou il faudra positionner l’indicateur, un « zero », n’indiquera pas d’indicateur, soit une
corde à vide
## One more thing!
Bon courage!!!